var fpbConfig; // devs can pass in a config object, else fallback to defaults

(function fpbModule(fpbConfig) {

  'use strict';

  /*
    FLOOR PLAN SEARCH MODULE
    Dependencies:
      scalyr ('sly', drastically improves template caching and speed,
        which is super helpful when trying to quickly append 500+ units to the unit
        list...)
      ng-sanitize ('ngSanitize', used to allow parsing of HTML in strings passed to the view)
  */
  angular.module('fpb', ['ngSanitize', 'sly']);

  /*
    Shims to add `classList` and `contains` to SVG elements b/c IE is an idiot.
    Found here: https://github.com/angular/angular/issues/6327#issuecomment-184608629
  */
  if (!("classList" in document.createElementNS("http://www.w3.org/2000/svg", "g"))) {
    var descr = Object.getOwnPropertyDescriptor(HTMLElement.prototype, 'classList');
    Object.defineProperty(SVGElement.prototype, 'classList', descr);
  }
  // Popper (Tippy dependency) needs `contains` to avoid bugging out in IE11
  if (!SVGElement.prototype.contains) {
    SVGElement.prototype.contains = function contains(node) {
      if (!(0 in arguments)) {
        throw new TypeError('1 argument is required');
      }
      do {
        if (this === node) {
          return true;
        }
      } while (node = node && node.parentNode);
      return false;
    }
  }

  ////////// GLOBAL VALUES //////////

  function defaultFor(arg, val) {
    return typeof arg !== 'undefined' ? arg : val;
  }

  /*
    Default options to run the module, accepts values from an external settings
    object. Below is one basic example, which would be set in app.js:

      var fpbConfig = {
        useFloorplanPricing: false,
        crumbIcons: {
          bldgIcon: 'some icon'
        },
        tippyConfig: {
          arrow: false,
          animateFill: false
        }
      }
  */
  fpbConfig.skipBuildingNav           = defaultFor(fpbConfig.skipBuildingNav, false);
  fpbConfig.useFloorplanPricing       = defaultFor(fpbConfig.useFloorplanPricing, true);
  fpbConfig.crumbIcons                = defaultFor(fpbConfig.crumbIcons, {});
  fpbConfig.crumbIcons.bldgIcon       = defaultFor(fpbConfig.crumbIcons.bldgIcon, '<i class="far fa-building"></i>');
  fpbConfig.crumbIcons.floorIcon      = defaultFor(fpbConfig.crumbIcons.floorIcon, '<i class="fas fa-layer-group"></i>');
  fpbConfig.crumbIcons.unitIcon       = defaultFor(fpbConfig.crumbIcons.unitIcon, '<i class="fal fa-th-large"></i>');
  fpbConfig.crumbIcons.searchIcon     = defaultFor(fpbConfig.crumbIcons.searchIcon, '<i class="fas fa-search"></i>');
  fpbConfig.tooltipConfig             = defaultFor(fpbConfig.tooltipConfig, {});
  fpbConfig.tooltipConfig.arrow       = defaultFor(fpbConfig.tooltipConfig.arrow, true);
  fpbConfig.tooltipConfig.touch       = defaultFor(fpbConfig.tooltipConfig.touch, false);
  fpbConfig.tooltipConfig.animateFill = defaultFor(fpbConfig.tooltipConfig.animateFill, true);
  fpbConfig.tooltipConfig.theme       = defaultFor(fpbConfig.tooltipConfig.theme, 'icon');
  fpbConfig.tooltipMarkup             = defaultFor(fpbConfig.tooltipMarkup, function (unit) {
    return [
      '<div class="tt-name"><strong>' + unit.name + '</strong></div>',
      '<div>' + unit.beds + 'bed / ' + unit.baths + 'bath</div>',
      '<div>' + unit.sqft + 'sqft</div>',
      '<div>' + (unit.is_available ? 'Available' : 'Unavailable') + '</div>',
      '<div>$' + unit.price + '</div>',
    ].join('');
  });

  angular.module('fpb').value('config', fpbConfig);

  /*
    CACHE
    Used by the whole module to keep track of what was recently clicked, data
    that's been loaded, active filters, etc. so that controllers can easily
    react to changes without needing tons of $watchers
  */
  angular.module('fpb').value('cache', {
    availableUnits: [],
    buildings: {},
    cacheKey: null,
    currBuilding: {},
    currFloorNum: null,
    filteredUnits: [],
    filterList: {
      floor: null,
      moveIn: null,
      plan: null,
      priceMinMax: [0, 99999]
    },
    filtersInitial: {},
    floorplans: null,
    floors: [],
    priceMax: 0,
    priceMin: 0,
    propKey: pid
  });

  angular.module('fpb').value('unitImages', {
    imageGroupName: 'unit views',
    imageList: []
  });

  angular.module('fpb').filter('numOrdinal', numOrdinalFilter);

  angular.module('fpb').factory('endpoint', endpointFactory);
  angular.module('fpb').factory('helpers', helpersFactory);
  angular.module('fpb').factory('logger', loggerFactory);
  angular.module('fpb').factory('radio', radioFactory);
  angular.module('fpb').factory('storage', storageFactory);

  angular.module('fpb').directive('dateInput', dateInputDirective);
  angular.module('fpb').directive('floorNavigator', floorNavigatorDirective);
  angular.module('fpb').directive('hoverHighlight', hoverHighlightDirective);
  angular.module('fpb').directive('modal', modalDirective);
  angular.module('fpb').directive('planNavigator', planNavigatorDirective);
  angular.module('fpb').directive('priceInput', priceInputDirective);

  angular.module('fpb').controller('BuildingNavController', BuildingNavController);
  angular.module('fpb').controller('CrumbController', CrumbController);
  angular.module('fpb').controller('FilterController', FilterController);
  angular.module('fpb').controller('FloorNavController', FloorNavController);
  angular.module('fpb').controller('LoaderController', LoaderController);
  angular.module('fpb').controller('PlanNavController', PlanNavController);
  angular.module('fpb').controller('ResultsController', ResultsController);
  angular.module('fpb').controller('UnitInfoController', UnitInfoController);
  angular.module('fpb').controller('VisualPathController', VisualPathController);


  ////////// FILTERS //////////

  /*
    ORDINAL NUMBERS FILTER
    Return given number in ordinal form (1st, 2nd, 3rd, etc)
  */
  function numOrdinalFilter() {
    return function (num) {
      var numMod10 = num % 10;
      var numMod100 = num % 100;

      if (numMod10 === 1 && numMod100 !== 11) {
        return num + 'st';
      }
      if (numMod10 === 2 && numMod100 !== 12) {
        return num + 'nd';
      }
      if (numMod10 === 3 && numMod100 !== 13) {
        return num + 'rd';
      }

      return num + 'th';
    }
  }


  ////////// SERVICES //////////

  /*
    DATA SERVICE
    Manages endpoint data retrieval
  */
  endpointFactory.$inject = ['$http'];
  function endpointFactory($http) {
    var service = {};

    service.getData = getData;

    return service;

    //////////

    function getData(query) {
      return $http.post('/graphql', JSON.stringify({ "query": query }));
    }
  }

  /*
    HELPER METHODS SERVICE
    Provides methods to controllers which abstract helpful, repeatedly-used
    operations into reusable + digestible pieces
  */
  function helpersFactory() {
    var service = {};

    service.endNumsFromString = findNumbers;
    service.findParentId      = findParent;
    service.isInArray         = findInArray;
    service.objectByValue     = findObject;
    service.roundToTen        = roundToTen;

    return service;

    //////////

    /*
      Returns object from an array based on desired key and arg values passed in
    */
    function findObject(arr, key, val) {
      for (var i = 0, len = arr.length; i < len; i++) {
        if (arr[i][key].toString() === val.toString()) {
          return arr[i];
        }
      }
    }

    /*
      Take the passed-in element and locate the nearest parent element with an ID
      matching the desired regex pattern
    */
    function findParent(el, pattern, return_string = false) {
      if (el.id.match(pattern)) {
        return !return_string ? el.id.replace(/\D/g, '') : el.id;
      } else {
        do {
          if (el.id && el.id.match(pattern)) {
            return !return_string ? el.id.replace(/\D/g, '') : el.id;
          }
        } while (el = el && el.parentNode);
      }
    }

    /*
      Grabs last group of digits from any string containing integers
      Examples:
        - Unit-1204 // returns 1204
        - 123-456   // returns 456
        - unit 992  // returns 992
    */
    function findNumbers(str) {
      return parseInt(str.substring(str.search(/(\d+)\D*$/)), 10);
    }

    function findInArray(array, searchValue) {
      var arrayLen = array.length;
      var c = 0;
      while (c < arrayLen) {
        if (array[c] === searchValue) {
          return c;
        }
        c++;
      }
    }

    function roundToTen(num) {
      return Math.round(parseInt(num, 10) / 10) * 10;
    }
  }

  /*
    LOGGING SERVICE
    Wraps console.log, providing a prefix + ability to activate via `logging` URL
    param. Adapted from this SO answer: https://stackoverflow.com/a/32928812/1505423
  */
  loggerFactory.$inject = ['$location'];
  function loggerFactory($location) {
    var logsActive = $location.search().logging === 'true';
    var emptyFunc = function () { };
    var loggers = {
      log:   logsActive ? console.log.bind(window.console) : emptyFunc,
      info:  logsActive ? console.info.bind(window.console) : emptyFunc,
      warn:  logsActive ? console.warn.bind(window.console) : emptyFunc,
      error: logsActive ? console.error.bind(window.console) : emptyFunc
    };

    return loggers;
  }

  /*
    MESSAGING SERVICE
    Wraps $rootScope event methods to abstract its usage out of the controllers.
  */
  radioFactory.$inject = ['$rootScope', 'logger'];
  function radioFactory($rootScope, logger) {
    var service = {};

    service.broadcast = eventEmitter;
    service.tuneIn    = eventReceiver;

    return service;

    //////////

    function eventEmitter(eventName, data) {
      $rootScope.$emit(eventName, data);
      logger.log(eventName, 'fired with:', data);
    }

    function eventReceiver(eventName, callback) {
      $rootScope.$on(eventName, callback);
    }
  }

  /*
    STORAGE SERVICE
    Manages browser data storage for the app.
    Example:
      var data = storage.grab('data');
  */
  storageFactory.$inject = ['$window', 'logger'];
  function storageFactory($window, logger) {
    var service = {};

    service.age   = dateData;
    service.grab  = getData;
    service.has   = findData;
    service.store = setData;

    return service;

    //////////

    function setData(key, value) {
      value['last_fetched'] = Math.floor(Date.now());
      $window.sessionStorage[key] = JSON.stringify(value);
    }

    function getData(key) {
      return JSON.parse($window.sessionStorage[key] || '{}');
    }

    function findData(key) {
      return $window.sessionStorage[key] ? true : false;
    }

    function dateData(key, days) {
      var msPerDay = 86400000;
      var storedData = this.grab(key);

      if (storedData.last_fetched) {
        var currentMs = Math.floor(Date.now());
        var expiresMs = (days * msPerDay) + storedData.last_fetched;
        var ageMs = currentMs - storedData.last_fetched;
        var ageDays = Math.round((ageMs / msPerDay) * 100) / 100;

        if (expiresMs > currentMs) {
          logger.warn('Data last fetched:', ageDays, 'days ago (expires after', days, 'days). Keeping current data and moving on...')
          return true;
        } else {
          logger.warn('Data last fetched:', ageDays, 'days ago (expires after', days, 'days). Need to get new data...');
        }
      } else {
        logger.warn('Data missing or age not known, fetching a new batch...')
      }
      return false;
    }
  }


  ////////// DIRECTIVES //////////

  dateInputDirective.$inject = ['radio'];
  function dateInputDirective(radio) {
    return {
      link: function (scope, element) {
        var picker = element[0];

        // details: https://flatpickr.js.org
        var calendar = flatpickr(picker, {
          altInput: true,
          altFormat: "F j, Y",
          dateFormat: "Y-m-d"
        });

        radio.tuneIn('filters:clear', function () {
          calendar.clear();
        });
      }
    };
  }

  /*
    Retrieve SVG markup for building's floor navigation view
  */
  floorNavigatorDirective.$inject = ['$q', '$http', '$compile', 'cache', 'radio'];
  function floorNavigatorDirective($q, $http, $compile, cache, radio) {
    return {
      restrict: 'A',
      link: function (scope, container, attrs) {
        radio.tuneIn('building:finished', function () {
          var bldgDir = cache.currBuilding.name.replace(/\s/g, '').toLowerCase();
          var getUrl = attrs['assetPath'] + bldgDir + '/floors.html';

          $q.race([$http.get(getUrl)])
            .then(function (response) {
              var svgMarkup = response.data;
              var el = angular.element(svgMarkup);

              $compile(el)(scope);
              container.html(el);
            });
        });
      }
    };
  }

  /*
    Enable highlighting of hovered elements whose `data-group` matches.
    Should work okay with both SVG + regular HTML elements. Structure
    should be something like...

    ```
      <parentElement hover-highlight="hover-highlight">
        <child1 id="123" />
        <child2 id="123" />
        <child3 id="456" />
        <child4 id="456" />
      </parentElement>
    ```

    Hovering over child1 should highlight child 1+2, and same for hovering over
    child2. child3 and 4 would act exactly the same, and so forth.
  */
  hoverHighlightDirective.$inject = ['helpers'];
  function hoverHighlightDirective(helpers) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var currEl = element[0];
        var children = currEl.querySelectorAll('[id^="unit-"]');
        var child;

        currEl.addEventListener('mouseover', handleHover, false);
        currEl.addEventListener('mouseout', handleHover, false);

        // Automatically set `data-group` num for parents whose child elements
        // lack the attr.
        for (var i = 0; i < children.length; i++) {
          child = children[i];
          if (!child.getAttribute('data-group')) {
            child.setAttribute('data-group', helpers.findParentId(child, /^unit-/));
          }
        }

        function handleHover(evt) {
          var groupNum = helpers.findParentId(evt.target, /^unit-/);

          if (groupNum) {
            var groupSelector = '[data-group="' + groupNum + '"]';
            var groups = document.querySelectorAll(groupSelector);

            for (var j = 0; j < groups.length; j++) {
              groups[j].classList.toggle('hovering');
            }
          }

        }
      }
    };
  }

  /*
    Allow usage of `modal: true` to setup modal popups anywhere in the FPB.
    Accepts `modal-type` as 'gallery' or 'iframe', defaults to 'image' if
    no type is specified.
  */
  function modalDirective() {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
        var modalSettings = {};

        modalSettings.type = 'image';

        switch (attr.modalType) {
          case 'gallery':
            modalSettings.delegate = 'a';
            modalSettings.gallery = { enabled: true };
            break;
          case 'iframe':
            modalSettings.type = 'iframe';
            break;
        }

        // The one unavoidable jQuery dependency in the whole FPB... (at least
        // until we make/find an alternative)
        element.magnificPopup(modalSettings);
      }
    }
  }

  /*
    Retrieve SVG markup for floor's unit navigation view
  */
  planNavigatorDirective.$inject = ['$q', '$http', '$compile', 'config', 'cache', 'radio', 'helpers'];
  function planNavigatorDirective($q, $http, $compile, config, cache, radio, helpers) {
    return {
      restrict: 'A',
      link: function (scope, svgElement, attrs) {
        // Don't really do anything until we have our data from the main endpoint call
        radio.tuneIn('building:finished', function (evt, id) {
          var allFloors = [];
          var bldgDir = cache.currBuilding.name.replace(/\s/g, '').toLowerCase();
          var compiled = [];
          var floorFile;
          var getUrl = attrs['planNavigator'] + bldgDir + '/floor_overviews/';

          cache.prevBldg = cache.prevBldg || null;

          // Create list of requests for each floor's SVG file
          for (var i = 0, len = cache.buildings[cache.cacheKey].floors.length; i < len; i++) {
            floorFile = 'floor-' + (i + 1) + '.svg';
            allFloors.push($http.get(getUrl + floorFile));
          }

          // Run requests to grab all floor SVG markup, then compile and add to DOM
          $q.all(allFloors)
            .then(function (responses) {
              var svgResponse;
              var svgResponseMarkup;

              for (var i = 0; i < responses.length; i++) {
                svgResponse = angular.element(responses[i].data)[0];
                svgResponseMarkup = new XMLSerializer().serializeToString(svgResponse);
                compiled.push(svgResponseMarkup);
              }

              compiled.unshift('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 500 600" width="100%" height="100%" xml:space="default" ng-click="planNav.unitClick($event)">');
              compiled.push('</svg>');

              buildSVGMarkup(compiled);
              setAvailability();
            });
        });

        function buildSVGMarkup(compiledMarkup) {
          var bldgName = cache.currBuilding.name.replace(/\s/g, '').toLowerCase();
          cache.prevBldg = cache.prevBldg || null;

          // Check to see if we're just re-showing the same building again, and if
          // so, skip :allthethings:
          if (!cache.prevBldg || cache.prevBldg !== bldgName) {
            var markup = compiledMarkup.join('');
            var el = angular.element(markup);

            cache.prevBldg = bldgName;

            $compile(el)(scope);

            // Add markup to the DOM once it's all compiled and hide the loader
            svgElement.html(el);
          }

          radio.broadcast('loader:hide');
        }

        function setAvailability() {
          var units = document.querySelectorAll('g[id^="unit-"]');
          var currBuildingUnits = cache.buildings[cache.cacheKey].available_units;
          var currUnit;
          var unitNum;
          var matchedUnit;

          for (var i = 0, len = units.length; i < len; i++) {
            currUnit = units[i];
            // grab markup unit num to match data unit num so we can mark SVG
            // polygon availability states
            unitNum = helpers.endNumsFromString(currUnit.id);
            matchedUnit = helpers.objectByValue(currBuildingUnits, 'unit_num', unitNum);

            if (matchedUnit) {
              currUnit.setAttribute('data-tippy-content', config.tooltipMarkup(matchedUnit));

              if (currUnit.classList) {
                currUnit.classList.add('available');
              } else {
                currUnit.className += ' available';
              }
            }
          }

          tippy(document.querySelectorAll('g[data-tippy-content]'), config.tippyConfig);
        }
      }
    };
  }

  function priceInputDirective(cache, radio) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        var slider = element[0];

        radio.tuneIn('all:complete', function () {
          // details: https://refreshless.com/nouislider
          noUiSlider.create(slider, {
            start: [cache.priceMin, cache.priceMax],
            behaviour: 'drag-tap',
            connect: true,
            margin: 400,
            range: {
              'min': cache.priceMin,
              'max': cache.priceMax
            },
            step: 200,
            tooltips: true,
            format: {
              to: function (value) {
                return '$' + Math.floor(value);
              },
              from: function (value) {
                return Math.floor(value);
              }
            }
          });

          slider.noUiSlider.on('change', function (prices) {
            // Convert array of strings to array of ints, set filters
            var newPriceMinMax = prices.map(function (p) { return Math.floor(p.replace(/[^\d\.-]+/g, '')) });
            cache.filterList.priceMinMax = newPriceMinMax;
          });
        });

        radio.tuneIn('filters:clear', function () {
          slider.noUiSlider.reset();
        });
      }
    };
  }


  ////////// CONTROLLERS //////////

  BuildingNavController.$inject = ['config', 'cache', 'helpers', 'radio', 'storage', 'endpoint', 'logger', 'unitImages'];
  function BuildingNavController(config, cache, helpers, radio, storage, endpoint, logger, unitImages) {
    var vm = this;

    vm.buildings = buildingsList() || [];
    vm.currBuilding = vm.buildings[0];
    vm.isVisible = !config.skipBuildingNav;
    vm.navClick = navClick;

    if (vm.buildings.length === 0) {
      logger.warn('CMS building data is missing or old data is cached in session storage');
      return;
    }

    activate();

    radio.tuneIn('building:return', showBuildingNav);

    //////////

    function activate() {
      if (config.skipBuildingNav) {
        pickBuilding();
      }
      return;
    }

    function buildBuildingData(bldgId) {
      var buildingData = {};
      var appData = '{\
          property(id: ' + cache.propKey + ') {\
            buildings(filter: {id: ' + bldgId + '}) {\
              name\
              floors {\
                id\
                name\
                units {\
                  address\
                  available_on_epoch\
                  description\
                  floorplan_id\
                  id\
                  is_available\
                  name\
                  rent_amount\
                }\
              }\
            }\
          }\
        }';

      cache.cacheKey = 'bldg' + bldgId;

      // If we haven't memoized the session cache, or if we force a refresh, hit
      // the endpoint... otherwise rely on current cache
      if (!cache.buildings[cache.cacheKey]) {
        radio.broadcast('loader:show');

        // Reset cached list values
        buildingData.floors = [];
        buildingData.units = [];
        buildingData.available_units = [];

        endpoint.getData(appData)
          .then(function (data) {
            var propData = data.data.data.property;

            if (propData) {
              var currBuilding = propData.buildings[0];
              var currFloor;
              var currFloorplan;
              var currUnit;
              var validDate;
              var isCommercial;
              var currFloorNum;

              // Loop through all building floors and create cached data for them
              for (var i = 0; i < currBuilding.floors.length; i++) {
                currFloor = currBuilding.floors[i];
                isCommercial = currFloor.name.search(/commercial/i) > -1;
                currFloorNum = helpers.endNumsFromString(currFloor.name);
                buildingData.floors.push({ floorNum: currFloorNum, isCommercial: isCommercial });

                // Loop through all units for current floor and create cached data for them
                for (var j = 0; j < currFloor.units.length; j++) {
                  currUnit = currFloor.units[j];
                  currFloorplan = helpers.objectByValue(cache.floorplans, 'id', currUnit.floorplan_id);
                  validDate = new Date().getTime() >= new Date(currUnit.available_on_epoch).getTime();

                  // Add additional unit keys to be cached
                  currUnit.unit_num = helpers.endNumsFromString(currUnit.name);
                  currUnit.bldg = currBuilding.name;
                  currUnit.floor_num = currFloorNum;
                  currUnit.bed_baths = currFloorplan.bedrooms + '+' + currFloorplan.bathrooms;
                  currUnit.beds = currFloorplan.bedrooms;
                  currUnit.baths = currFloorplan.bathrooms;
                  currUnit.sqft = currFloorplan.sqft;
                  currUnit.price = config.useFloorplanPricing ? currFloorplan.cached_price : currUnit.rent_amount;
                  currUnit.gallery = unitImages.imageList.filter(function (image) {
                    return image.imageIds.indexOf(currUnit.unit_num) > -1 ? true : false;
                  });

                  // Create array of available units to help build SVG markup
                  // POSSIBLY ABLE TO REMOVE; might be able to use 'available units'
                  // from unit search list for this
                  if (currUnit.is_available && validDate) {
                    buildingData.available_units.push(currUnit);
                  }

                  buildingData.units.push(currUnit);
                }
              }
            }

            // Store compiled building data in memoized cache
            cache.buildings[cache.cacheKey] = buildingData;

            finishBuildingData();
          });
      } else {
        finishBuildingData();
      }

      logger.log('query method complete', cache);
    }

    function buildingsList() {
      var queryData = '{\
        property(id: ' + cache.propKey + ') {\
          buildings {\
            id\
            name\
          }\
        }\
      }';

      // Avoid asking the endpoint for the building list if possible
      if (!storage.has('ll_fpb_buildings')) {
        endpoint.getData(queryData)
          .then(function (data) {
            if (!data.errors) {
              vm.buildings = data.data.data.property.buildings;
              storage.store('ll_fpb_buildings', vm.buildings);
            } else {
              logger.log('buildingNav error:', data.errors, queryData);
            }
          });
      }

      return storage.grab('ll_fpb_buildings');
    }

    function finishBuildingData() {
      radio.broadcast('building:finished');
    }

    function navClick(evt) {
      var svgBldgId = helpers.findParentId(evt.target, /building\s?\d+/ig, true);
      var currBuilding;
      var bldgName;

      // Check clicked item group ID against building list for a name string match
      // to set currBuilding
      if (vm.buildings.length > 0) {
        for (var i = 0; i < vm.buildings.length; i++) {
          currBuilding = vm.buildings[i];
          bldgName = currBuilding.name.replace(/\s/g, '').toLowerCase();

          if (bldgName === svgBldgId) {
            vm.currBuilding = currBuilding;
            break; // Stop looping as soon as possible
          }
        }

        pickBuilding();
      } else {
        logger.warn(svgBldgId, 'clicked - no CMS building data present');
      }
    }

    function pickBuilding() {
      vm.isVisible = false;
      cache.currBuilding = vm.currBuilding;
      buildBuildingData(vm.currBuilding.id);
      radio.broadcast('building:pick', vm.currBuilding);
    }

    function showBuildingNav() {
      vm.isVisible = true;
    }
  }

  CrumbController.$inject = ['config', 'cache', 'radio'];
  function CrumbController(config, cache, radio) {
    var vm = this;
    var bldgIcon = '<i class="far fa-building"></i>';
    var floorIcon = '<i class="fas fa-layer-group"></i>';
    var unitIcon = '<i class="fal fa-th-large"></i>';
    var searchIcon = '<i class="fas fa-search"></i>';

    vm.crumbList = [];
    vm.depth = 0;
    vm.returnClick = returnClick;

    radio.tuneIn('building:return', dropCrumb);
    radio.tuneIn('mainView:return', dropCrumb);
    radio.tuneIn('unitList:return', dropCrumb);
    radio.tuneIn('building:pick', function () {
      if (!config.skipBuildingNav) {
        addCrumb({ title: config.crumbIcons.bldgIcon + ' ' + cache.currBuilding.name, evt: 'building:return' });
      }
    });
    radio.tuneIn('unit:pick', function (evt, unitInfo) {
      addCrumb({ title: config.crumbIcons.floorIcon + ' Floor ' + cache.currFloorNum, evt: 'mainView:return' });

      if (typeof unitInfo === 'number' && (unitInfo % 1) === 0) {
        addCrumb({ title: config.crumbIcons.unitIcon + ' Unit ' + unitInfo, evt: '' });
      } else {
        addCrumb({ title: config.crumbIcons.unitIcon + ' ' + unitInfo.unit.name, evt: '' });
      }
    });
    radio.tuneIn('unitList:pick', function (evt, unitInfo) {
      addCrumb({ title: config.crumbIcons.searchIcon + ' Search Results List', evt: 'unitList:return' });
      addCrumb({ title: config.crumbIcons.unitIcon + ' ' + unitInfo.unit.name, evt: '' });
    });
    radio.tuneIn('filters:submit', function () {
      clearCrumbs();
      if (config.skipBuildingNav) {
        addCrumb({ title: 'Hide Results', evt: 'mainView:return' });
      } else {
        addCrumb({ title: config.crumbIcons.bldgIcon + ' Building Selector', evt: 'building:return' });
      }
    });
    radio.tuneIn('unit:swap', function (evt, unitInfo) {
      dropCrumb();
      addCrumb({ title: config.crumbIcons.unitIcon + ' ' + unitInfo.unit.name, evt: '' });
    });

    //////////

    function addCrumb(crumb) {
      vm.crumbList.push(crumb);
      updateDepth();
    }

    function clearCrumbs() {
      vm.crumbList = [];
    }

    function dropCrumb(evt, amt) {
      var amount = amt || 1;

      vm.crumbList.splice(-(amount), amount);
      updateDepth();
    }

    function returnClick(depth, evt) {
      if (evt !== '') {
        radio.broadcast(evt, depth);
      }
    }

    function updateDepth() {
      vm.depth = vm.crumbList.length;
    }
  }

  FilterController.$inject = ['$q', 'cache', 'radio'];
  function FilterController($q, cache, radio) {
    var vm = this;
    var plansLoaded = $q.defer();
    var unitsLoaded = $q.defer();

    vm.clear = clear;
    vm.dateChanged = dateChanged;
    vm.filterList;
    vm.floors;
    vm.moveDate;
    vm.plans;
    vm.submit = submit;

    // Listen for other controllers' data calls to complete...
    radio.tuneIn('floorplans:complete', function () {
      plansLoaded.resolve();
    });
    radio.tuneIn('unitsList:complete', function () {
      unitsLoaded.resolve();
    });

    // Only build filters after we have all our data
    $q.all([unitsLoaded.promise, plansLoaded.promise]).then(function () {
      buildFilters();
      radio.broadcast('all:complete');
    });

    //////////

    function buildFilters() {
      var uniquePlans = new Set(); // Use due to innate uniqueness
      var planList = [];
      var planRooms;

      // Construct data for beds + baths dropdown
      cache.floorplans.forEach(function (plan) {
        uniquePlans.add(plan.bedrooms + '+' + plan.bathrooms);
      });
      uniquePlans.forEach(function (plan) {
        planRooms = plan.split('+');
        planList.push({ name: planRooms[0] + 'bd / ' + planRooms[1] + 'ba', value: plan });
      });

      // Final assembly for all filters to be used by the view
      vm.filterList = cache.filterList;
      vm.plans = planList;
      vm.floors = cache.floors;
      vm.moveDate = vm.filterList.moveIn;
      cache.filterList.priceMinMax = [cache.priceMin, cache.priceMax];
      cache.filtersInitial.priceMinMax = [cache.priceMin, cache.priceMax];
    }

    function clear() {
      for (var filter in vm.filterList) {
        vm.filterList[filter] = cache.filtersInitial[filter] ? cache.filtersInitial[filter] : null;
      }
      radio.broadcast('filters:clear');
    }

    function dateChanged() {
      cache.filterList.moveIn = vm.moveDate;
    };

    function submit() {
      radio.broadcast('filters:submit');
    }
  }

  FloorNavController.$inject = ['config', 'cache', 'radio', 'helpers', 'numOrdinalFilter'];
  function FloorNavController(config, cache, radio, helpers, numOrdinalFilter) {
    var vm = this;

    vm.floorActive = floorActive;
    vm.floorClick = floorClick;
    vm.floorDropdownClick = floorDropdownClick;
    vm.hideNav = hideNav;
    vm.isVisible = config.skipBuildingNav;
    vm.selectedFloor = 1;

    radio.tuneIn('building:pick', function () {
      vm.isVisible = true;
    });
    radio.tuneIn('building:finished', function () {
      buildDropdown();
    });
    radio.tuneIn('mainView:return', function () {
      vm.isVisible = true;
    });
    radio.tuneIn('unit:pick', vm.hideNav);
    radio.tuneIn('unitList:pick', vm.hideNav);
    radio.tuneIn('building:return', vm.hideNav);

    //////////

    function buildDropdown() {
      var floors = cache.buildings[cache.cacheKey].floors.sort(function (a, b) { return a.floorNum - b.floorNum; });;
      var floorList = [];
      var floorNum;

      for (var floor in floors) {
        floorNum = floors[floor].floorNum;
        floorList.push({ name: numOrdinalFilter(floorNum) + ' Floor', value: floorNum });
      }

      vm.floors = floorList;
    }

    function floorActive(num) {
      return num === cache.currFloorNum;
    }

    function floorClick(evt) {
      var floorNum = helpers.findParentId(evt.target, /floor\s?\d+/ig);

      if (floorNum) {
        cache.currFloorNum = parseInt(floorNum, 10);
        radio.broadcast('floor:pick', cache.currFloorNum);
      }
    }

    function floorDropdownClick() {
      cache.currFloorNum = vm.selectedFloor;
      radio.broadcast('floor:pick', cache.currFloorNum);
    }

    function hideNav() {
      vm.isVisible = false;
    }
  }

  LoaderController.$inject = ['radio'];
  function LoaderController(radio) {
    var vm = this;
    vm.isVisible = false;

    radio.tuneIn('loader:show', function () {
      vm.isVisible = true;
    });
    radio.tuneIn('loader:hide', function () {
      vm.isVisible = false;
    });
  }

  PlanNavController.$inject = ['cache', 'radio', 'logger', 'storage', 'endpoint', 'helpers'];
  function PlanNavController(cache, radio, logger, storage, endpoint, helpers) {
    var vm = this;

    vm.currFloor = 1;
    vm.floorplanList = floorplanList;
    vm.hideUnitsNav = hideUnitsNav;
    vm.isVisible = false;
    vm.pickFloor = pickFloor;
    vm.switchFloors = switchFloors;
    vm.unitClick = unitClick;

    radio.tuneIn('building:finished', pickInitialFloor);
    radio.tuneIn('floor:pick', vm.switchFloors);
    radio.tuneIn('unit:pick', vm.hideUnitsNav);
    radio.tuneIn('building:return', vm.hideUnitsNav);
    radio.tuneIn('mainView:return', function () {
      vm.isVisible = true;
    });

    //////////

    function floorplanList() {
      var queryData = '{\
        property(id: ' + cache.propKey + ') {\
          floorplans {\
            id\
            name\
            bedrooms\
            bathrooms\
            image\
            sqft\
            cached_price\
            apply_now_url\
            brochure_pdf\
          }\
        }\
      }';

      // Avoid calling for the floorplans list if possible
      if (storage.has('ll_fpb_floorplans')) {
        finishFloorplanData(storage.grab('ll_fpb_floorplans'));
      } else {
        endpoint.getData(queryData)
          .then(function (data) {
            // Yo dawg, I heard you like nested objects...
            var propFloorplans = data.data.data.property.floorplans;
            storage.store('ll_fpb_floorplans', propFloorplans);
            finishFloorplanData(propFloorplans);
          });
      }
    }

    function finishFloorplanData(floorplanData) {
      cache.floorplans = floorplanData;
      if (!floorplanData.length > 0) {
        logger.warn('CMS floorplans data is missing or old data is cached in session storage');
      }
      radio.broadcast('floorplans:complete');
    }

    function hideUnitsNav() {
      vm.isVisible = false;
    }

    function pickFloor(chosenFloor) {
      return chosenFloor === cache.currFloorNum;
    }

    function pickInitialFloor() {
      var bldgFloors = cache.buildings[cache.cacheKey].floors;
      var currFloor;

      bldgFloors.sort(function (f1, f2) { return f1.floorNum - f2.floorNum });

      // Find the first non-commercial floor and select it when building
      // is chosen
      for (var floor in bldgFloors) {
        currFloor = bldgFloors[floor];
        if (!currFloor.isCommercial) {
          vm.currFloor = currFloor.floorNum;
          break;
        }
      }

      vm.isVisible = true;
      cache.currFloorNum = vm.currFloor;
      radio.broadcast('floor:pick', cache.currFloorNum);
    }

    function switchFloors(evt, floorId) {
      if (floorId && vm.currFloor !== floorId) {
        vm.currFloor = floorId;
      }
      vm.isVisible = true;
    }

    function unitClick(evt) {
      var unitId = helpers.findParentId(evt.target, /unit-\w?\d+/g);

      if (unitId) {
        radio.broadcast('unit:pick', parseInt(unitId, 10));
      }
    }
  }

  ResultsController.$inject = ['config', 'cache', 'unitImages', 'radio', 'endpoint', 'logger', 'helpers', 'numOrdinalFilter'];
  function ResultsController(config, cache, unitImages, radio, endpoint, logger, helpers, numOrdinalFilter) {
    var vm = this;

    vm.isVisible = false;
    vm.filteredUnits;
    vm.unitList;
    vm.pickUnit = pickUnit;
    vm.getData = getData;

    radio.tuneIn('building:return', function () {
      vm.isVisible = false;
    });
    radio.tuneIn('mainView:return', function () {
      vm.isVisible = false;
    });
    radio.tuneIn('unitList:return', function () {
      vm.isVisible = true;
    });
    radio.tuneIn('filters:submit', function () {
      filterResults();
      vm.isVisible = true;
    });
    radio.tuneIn('filters:clear', function () {
      vm.filteredUnits = vm.unitList;
      updateCount();
    });
    radio.tuneIn('unit:pick', function () {
      vm.isVisible = false;
    });
    radio.tuneIn('unitList:pick', function () {
      vm.isVisible = false;
    });

    //////////

    function filterResults() {
      var activeFilterCount = 0;
      var moveInEpoch = 0;
      var unitAttrs;
      var currUnit;
      var filter;
      var unit;

      vm.filteredUnits = [];

      // How many filters are currently active? If unit passes tests for all active
      // filters, show it in the final filtered list
      for (filter in cache.filterList) {
        if (cache.filterList[filter] !== null && cache.filterList[filter] !== '') {
          activeFilterCount++;
        }
      }

      // Apply filters to units
      for (unit in vm.unitList) {
        unitAttrs = [];
        currUnit = vm.unitList[unit];

        if (cache.filterList.plan) {
          if (currUnit.bed_baths === cache.filterList.plan) {
            unitAttrs.push('hasBedsNBaths');
          }
        }
        if (cache.filterList.floor) {
          if (currUnit.floor_num === cache.filterList.floor) {
            unitAttrs.push('onCurrentFloor');
          }
        }
        if (cache.filterList.moveIn) {
          moveInEpoch = new Date(cache.filterList.moveIn).getTime() / 1000;
          if (currUnit.available_on_epoch <= moveInEpoch) {
            unitAttrs.push('moveInIsBeforeDate');
          }
        }
        if (cache.filterList.priceMinMax) {
          if (currUnit.price >= cache.filterList.priceMinMax[0] && currUnit.price <= cache.filterList.priceMinMax[1]) {
            unitAttrs.push('priceWithinBounds');
          }
        }

        // If unit's attrs meet all active filter criteria, show it
        if (unitAttrs.length === activeFilterCount) {
          currUnit.idx = vm.filteredUnits.length;
          vm.filteredUnits.push(currUnit);
        }
      }

      cache.filteredUnits = vm.filteredUnits;
      updateCount();
    }

    function getData() {
      var queryData = '{\
        property(id: ' + cache.propKey + ') {\
          price_min\
          price_max\
          floors {\
            name\
          }\
          image_groups(filter:{name_is:"' + unitImages.imageGroupName + '"}) {\
            images {\
              title\
              url\
              url_thumb\
            }\
          }\
          units(filter:{available_only: true}) {\
            id\
            name\
            address\
            rent_amount\
            available_on_epoch\
            floorplan_id\
            description\
            floor {\
              name\
              building {\
                name\
              }\
            }\
            floorplan {\
              name\
              sqft\
              image\
              bedrooms\
              bathrooms\
              cached_price\
              apply_now_url\
              brochure_pdf\
            }\
          }\
        }\
      }';

      radio.broadcast('loader:show');

      endpoint.getData(queryData)
        .then(function (data) {
          if (!data.errors) {
            var propData = data.data.data.property;
            var propFloors = [];
            var floorNum;
            var currUnit;
            var currImg;

            if (!propData.price_min && !propData.price_max) {
              logger.warn('Property price_min/price_max are null... your property might be missing its floorplan + unit data');
            }

            cache.priceMin = helpers.roundToTen(propData.price_min) || cache.filterList.priceMinMax[0];
            cache.priceMax = helpers.roundToTen(propData.price_max) || cache.filterList.priceMinMax[1];

            // Create list of images to search their titles for unit IDs later
            if (propData.image_groups && propData.image_groups.length > 0) {
              for (var img in propData.image_groups[0].images) {
                currImg = propData.image_groups[0].images[img];
                if (currImg.title) {
                  currImg.imageIds = currImg.title.split(' ').map(function (num) { return +num });
                  unitImages.imageList.push(currImg);
                }
              }
            }

            // Build + sort floors array for filter dropdown
            if (propData.floors.length > 0) {
              for (var floor in propData.floors) {
                floorNum = helpers.endNumsFromString(propData.floors[floor].name);
                if (propFloors.indexOf(floorNum) === -1) {
                  propFloors.push(floorNum);
                }
              }

              propFloors.sort(function (a, b) { return a - b; });

              for (var floor in propFloors) {
                cache.floors.push({ name: numOrdinalFilter(propFloors[floor]) + ' Floor', value: propFloors[floor] });
              }
            } else {
              logger.warn('CMS floors data is missing')
            }

            if (propData.units.length > 0) {
              for (var unit in propData.units) {
                currUnit = propData.units[unit];
                currUnit.unit_num = helpers.endNumsFromString(currUnit.name);
                currUnit.floor_num = helpers.endNumsFromString(currUnit.floor.name);
                currUnit.bldg = currUnit.floor.building.name;
                currUnit.bed_baths = currUnit.floorplan.bedrooms + '+' + currUnit.floorplan.bathrooms;
                currUnit.price = config.useFloorplanPricing ? currUnit.floorplan.cached_price : currUnit.rent_amount;
                currUnit.gallery = unitImages.imageList.filter(function (image) {
                  return image.imageIds.indexOf(currUnit.unit_num) > -1 ? true : false;
                });
              }

              cache.availableUnits = propData.units;
              vm.unitList = cache.availableUnits;
              vm.filteredUnits = vm.unitList;
              updateCount();
            } else {
              logger.warn('CMS units data is missing');
            }
            radio.broadcast('unitsList:complete');
          } else {
            logger.log('unitsList error:', data.errors);
          }

          radio.broadcast('loader:hide');
        });
    }

    function pickUnit(unit, idx) {
      radio.broadcast('unitList:pick', { idx: idx, arrayLen: vm.filteredUnits.length, unit: unit, src: 'filters' });
    }

    function updateCount() {
      vm.visibleUnits = vm.filteredUnits.length;
    }
  }

  UnitInfoController.$inject = ['cache', 'unitImages', 'radio', 'logger', 'helpers'];
  function UnitInfoController(cache, unitImages, radio, logger, helpers) {
    var vm = this;
    var src;

    vm.floorplanData;
    vm.hasNextUnit;
    vm.hasPrevUnit;
    vm.hideUnitView = hideUnitView;
    vm.isVisible = false;
    vm.nextPrevClick = nextPrevClick;
    vm.soloUnitNav;
    vm.srcLength;
    vm.unitChosen = unitChosen;
    vm.unitData;
    vm.unitIdx;

    radio.tuneIn('unit:pick', vm.unitChosen);
    radio.tuneIn('unitList:pick', vm.unitChosen);
    radio.tuneIn('unitList:return', vm.hideUnitView);
    radio.tuneIn('mainView:return', vm.hideUnitView);
    radio.tuneIn('building:return', vm.hideUnitView);
    radio.tuneIn('filters:submit', vm.hideUnitView);

    //////////

    function hideUnitView() {
      vm.isVisible = false;
    }

    function nextPrevClick(dir) {
      dir === 'next' ? vm.unitIdx++ : vm.unitIdx--;
      vm.hasPrevUnit = vm.unitIdx > 0 ? true : false;
      vm.hasNextUnit = vm.unitIdx + 1 < vm.srcLength ? true : false;
      vm.unitData = helpers.objectByValue(src, 'idx', vm.unitIdx);
      vm.floorplanData = helpers.objectByValue(cache.floorplans, 'id', vm.unitData.floorplan_id);

      radio.broadcast('unit:swap', { dir: dir, unit: vm.unitData });
    }

    function unitChosen(evt, pickedUnit) {
      if (typeof pickedUnit === 'object') {
        vm.srcLength = pickedUnit.arrayLen;
        vm.hasNextUnit = pickedUnit.idx + 1 < vm.srcLength ? true : false;
        vm.hasPrevUnit = pickedUnit.idx > 0 ? true : false;
        vm.soloUnitNav = false;

        if (pickedUnit.src === 'visual') {
          src = cache.buildings[cache.cacheKey].units;
        } else if (pickedUnit.src === 'filters') {
          vm.soloUnitNav = true;
          src = cache.filteredUnits;
        }

        vm.unitIdx = pickedUnit.idx;
        vm.unitData = pickedUnit.unit;
        cache.currUnitId = helpers.endNumsFromString(pickedUnit.unit.name);
      } else {
        vm.hasNextUnit = false;
        vm.hasPrevUnit = false;
        cache.currUnitId = parseInt(pickedUnit, 10);
        vm.unitData = helpers.objectByValue(cache.buildings[cache.cacheKey].units, 'unit_num', cache.currUnitId);
      }

      logger.log('data for clicked unit:', vm.unitData, cache);

      if (vm.unitData) {
        vm.floorplanData = helpers.objectByValue(cache.floorplans, 'id', vm.unitData.floorplan_id);
        vm.isVisible = true;
      } else {
        logger.log('No unit found matching that identifier');
      }
    }
  }

  VisualPathController.$inject = ['cache', 'radio'];
  function VisualPathController(cache, radio) {
    var vm = this;

    vm.building;
    vm.isVisible = true;
    vm.pickUnit = pickUnit;
    vm.showUnitsList = false;
    vm.toggleAccordions = toggleAccordions;
    vm.unitList;

    radio.tuneIn('floor:pick', updateList);
    radio.tuneIn('building:pick', updateBldg);
    radio.tuneIn('building:finished', function () {
      vm.showUnitsList = true;
    });
    radio.tuneIn('building:return', function () {
      vm.isVisible = true;
      vm.showUnitsList = false;
    });
    radio.tuneIn('mainView:return', function () {
      vm.isVisible = true;
      vm.showUnitsList = true;
    });
    radio.tuneIn('unit:pick', function () {
      vm.showUnitsList = false;
    });
    radio.tuneIn('filters:submit', function () {
      vm.isVisible = false;
    });

    //////////

    function pickUnit(unit, idx) {
      radio.broadcast('unit:pick', { idx: idx, arrayLen: vm.unitList.length, unit: unit, src: 'visual' });
    }

    function toggleAccordions(evt) {
      // Dirty way to do this... maybe move to a directive?
      var rows = document.querySelectorAll('.table__row');
      var row = evt.currentTarget;

      Array.prototype.forEach.call(rows, function (row, idx) {
        if (row.classList.contains('visible')) {
          row.classList.remove('visible');
        }
      });

      row.classList.toggle('visible');
    }

    function updateBldg() {
      vm.building = cache.currBuilding.name;
    }

    function updateList() {
      var units = cache.buildings[cache.cacheKey].units;
      var unitList = [];
      var currUnit;
      var unit;

      for (unit in units) {
        currUnit = units[unit];
        if (currUnit.floor_num === cache.currFloorNum) {
          unitList.push(currUnit);
        }
      }

      vm.unitList = unitList;
    }
  }

})(fpbConfig || (fpbConfig = {}));